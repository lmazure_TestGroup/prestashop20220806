package prestashoptest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebDriverException;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import prestashoptest.helpers.StringsHelper;
import prestashoptest.pageobjects.HomePageObject;
import prestashoptest.pageobjects.IdentityPageObject;
import prestashoptest.pageobjects.SignInPageObject;
import prestashoptest.seleniumtools.PageObjectBase;

public class SetupTeardown {

    private static List<User> usersToBeDeleted;

    /**
     * test setup
     */
    @Before
    public static void setup() {
        PageObjectBase.setBrowser(PageObjectBase.Browser.FIREFOX);
        PageObjectBase.setHost("http://localhost:8080/fr");
        usersToBeDeleted = new ArrayList<>();
    }

    /**
     *
     * test teardown
     *
     * @param scenario
     */
    @After
    public static void teardown(final Scenario scenario) {

        // create snapshot in case of a test failure
        if (scenario.isFailed()) {
            final byte[] screenshot = PageObjectBase.getScreenshot();
            final String screenshotName = "screenshot " + scenario.getName() + " (line " + scenario.getLine() + ")";
            scenario.attach(screenshot,"image/png", screenshotName);
        }

        // delete the users created during the test
        deleteUsers();

        // close the browser
        PageObjectBase.quit();
    }

    public static void recordUserToBeDeleted(final String firstName,
                                             final String lastName,
                                             final String email,
                                             final String password) {
        usersToBeDeleted.add(new User(firstName, lastName, email, password));
    }

    private static void deleteUsers() {
        Collections.reverse(usersToBeDeleted);
        for (final User user: usersToBeDeleted) {
            deleteUser(user);
        }
        usersToBeDeleted = null;
    }

    private static void deleteUser(final User user) {
        // There is no easy way to delete the user
        // either we pay for a Prestashop module to add the "delete my account" button on the "My personal information" page
        // or we use the REST API, but we need to manage the token
        // or we delete the user row in the database
        // or whatever you can imagine
        // So, for the time being, we simply change the email address

        final String newEmail = "_to_be_deleted_" + StringsHelper.generateRandomId() + "_" + user.getEmail();

        try {

            final HomePageObject homePage = new HomePageObject();
            homePage.goTo();
            if (homePage.isSignedIn()) {
                if (!homePage.getDisplayedUserName().equals(user.getFirstName() + " " + user.getLastName())) {
                    // the current user is not the one we want to delete, we change
                    homePage.signOut();
                    final SignInPageObject signInPage = new SignInPageObject();
                    signInPage.goTo();
                    signInPage.signIn(user.getEmail(), user.getPassword());
                }
            } else {
                final SignInPageObject signInPage = new SignInPageObject();
                signInPage.goTo();
                signInPage.signIn(user.getEmail(), user.getPassword());
            }

            final IdentityPageObject identityPage = new IdentityPageObject();
            identityPage.goTo();
            identityPage.fillEmail(newEmail);
            identityPage.fillPassword(user.getPassword());
            identityPage.acceptPrivacyPolicy();
            identityPage.acceptGdpr();
            identityPage.submitForm();

        } catch (final WebDriverException e) {
            System.err.println("Failed to delete user " + user.getEmail());
            e.printStackTrace();
        }
    }

    private static class User {
        private final String firstName;
        private final String lastName;
        private final String email;
        private final String password;
        public User(final String firstName,
                    final String lastName,
                    final String email,
                    final String password) {
            this.firstName= firstName;
            this.lastName= lastName;
            this.email = email;
            this.password = password;
        }
        public String getFirstName() {
            return this.firstName;
        }
        public String getLastName() {
            return this.lastName;
        }
        public String getEmail() {
            return this.email;
        }
        public String getPassword() {
            return this.password;
        }
    }
}
