package prestashoptest.pageobjects;

import java.util.regex.Pattern;

import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Catalog page
 *
 */
public class CatalogPageObject extends PageObjectBase {

    public CatalogPageObject() {
        super(Pattern.compile("\\d+-\\w+"));
    }

    /**
     * Select a product in the catalog
     *
     * @param product
     */
    public void selectProduct(final String product) {
        clickElement(SelectBy.XPATH, "//img[@alt='" + product + "']");
    }
}
